# RhizoSMASH development

This repository contains files used in tuning of rhizoSMASH.

- Construction of knownClusterBlast database
- Blast tool for pathway genes
- Cluster tuning loops