rKnownCGC: known cluster blast database for rhizoSMASH
======================================================

Release 1.0
-----------

* 69 known gene clusters (88 total entries including subloci)
* 1~3 known gene clusters for each detection rules in rhizoSMASH v0.2.0

Reproduce
---------

To reproduce the database on your local computer, you need python (>= 3.6) and Biopython installed. Clone the repo or folder, move to `rKnownCGC/`, then

```bash
python build-database.py locus-list.txt genbank/ fasta/
```

Details
-------

Know cluster information are stored in the text file `locus-list.txt`. In **release 1.0**, This file contains a tab-deliminated table of 8 columns/fields. These fields are:

1. **rKnownCGC ID** A code starts with "RCGC" and follows with an indentical number. For whole loci, the code ends with 0. For some gene cluster loci with subloci, their subloci only have the final digit different.
2. **Sequence source accession** Accession to fetch sequences from NCBI nuccore.
3. **Short name for organism**
4. **Metabolite name**
5. **Metabolite category** For **release 1.0**, they should be one of SU (carbohydrate), OA (organic acid), AA (amino acid), AM (amine), or SE (secondary metabolite).
6. **Locus name**
7. **Gene names** or **locus tags**, separated by comma. They are used to retrieve CDS and peptide sequences from source records.
8. **Assigned gene names** Some gene names are re-assigned to make homogenous loci having consistent nominology.

Output in `fasta/` contains a `proteins.fasta` file and a `clusters.txt` file. They are formated in the way that the antiSMASH core in rhizoSMASH can recognize as known cluster blast database.

Output in `genbank/` contains chopped genbank files of each gene cluster locus. They can be used as reference cluster sets when running bigSCAPE.

For example, this record in `locus-list.txt`

```
RCGC000490	NC_002947	P. putida KT2240	glutarate	OA	gcd	PP_RS00810,PP_RS00815,PP_RS00820	gcdR,gcdG,gcdH
```

Stands for a gene cluster locus encoding the glutarate utilization enzymes gcdRGH in Pseudomonas putida str. KT2240. These genes can be found in RefSeq record `NC_002947` using locus tag PP_RS00810, PP_RS00815 and PP_RS00820. In rKnownCGC database, this known gene cluster is associated with an unique ID `RCGC000490`.