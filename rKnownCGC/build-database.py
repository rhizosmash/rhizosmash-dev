#!/usr/bin/env python3
"""Load rKnownCGC definition files and generate database

Usage:
    build-database.py [-h] [-i DEFINITIONS] [-s SOURCE_DIR] [-g GENBANK_DIR]
    [-f FASTA_DIR]

options:
  -h, --help show this help message and exit
  -i/--definitions DEFINITIONS
             The folder definition yaml files are in(default: definitions/)
  -s/--source-dir SOURCE_DIR
             The folder to put original sequences from NCBI(default: genbank/)
  -g/--genbank-dir GENBANK_DIR
             The folder to save genbank format database(default: source/)
  -f/--fasta-dir FASTA_DIR
             The folder to save fasta format database(default: fasta/)
"""

from os import listdir
from os.path import exists, isdir, isfile
from typing import TextIO, List, Generator
from sys import stderr

if __name__ == '__main__':
    ## parse command line args
    import argparse
    parser = argparse.ArgumentParser(
        description="Load rKnownCGC definition files and generate database")
    parser.add_argument("-i", "--definitions",
        type=str, default="definitions/",
        help="The folder definition yaml files are in"
             "(default: definitions/)")
    parser.add_argument("-s", "--source-dir",
        type=str, default="source/",
        help="The folder to put original sequences from NCBI"
             "(default: source/)")
    parser.add_argument("-g", "--genbank-dir",
        type=str, default="genbank/",
        help="The folder to save genbank format database"
             "(default: source/)")
    parser.add_argument("-f", "--fasta-dir",
        type=str, default="fasta/",
        help="The folder to save fasta format database"
             "(default: fasta/)")

    options = parser.parse_args()
    definition_dir  = options.definitions
    source_dir      = options.source_dir
    genbank_dir     = options.genbank_dir
    fasta_dir       = options.fasta_dir

    ## validate args
    for dd in [definition_dir, source_dir, genbank_dir, fasta_dir]:
        if not exists(dd) or not isdir(dd):
            print(f"ERROR {dd} not exist or is not a directory", stderr)
            exit(1)

from Bio import Entrez, SeqIO
Entrez.email = "yuze.li@wur.nl"
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import ExactPosition, SeqFeature, FeatureLocation
import yaml

def print_features(record:SeqRecord):
    for f in record.features:
        print(f)

def load_record_from_ncbi(accession:str, source:str) -> SeqRecord:
    """Run efetch to load genbank record from NCBI nuccore
    Parameters:
        accession   (str) NCBI nucleotide accession
                            (e.g. NC_000913 for E. coli K-12)
        source      (str) directory where source genbank files are saved
    Returns a SeqRecord class object
    """
    source_path = f"{source}/{accession}.gbk"
    if exists(source_path):
        record = SeqIO.read(source_path, "gb")
    else:
        with Entrez.efetch(db="nuccore", id=accession,
                           rettype="gbwithparts", retmode="text") as f:
            record = SeqIO.read(f, "gb")
        SeqIO.write(record, source_path, "gb")
    return record

def find_identifier(feature, region_def: dict):
    """Returns feature identifier found in region def"""
    if feature.type not in ["gene", "CDS"]:
        return None
    # catch identifiers
    identifiers = []
    qualifiers = feature.qualifiers
    for q in ["gene", "locus_tag"]:
        if q in qualifiers:
            identifiers += qualifiers[q]
    # find identifiers
    for i in identifiers:
        if i in region_def["mapping"]:
            return i
    return None

def clip_region(record: SeqRecord, region_def: dict) -> SeqRecord:
    """Clip a region from a genbank SeqRecord"""

    found_features = [f for f in record.features
                        if find_identifier(f, region_def) is not None]
    if len(found_features) == 0:
        print("ERROR "
            f"all genes in {region_def['id']} are not found in "
            f"record {record.id}", file=stderr)
    if len(found_features) < len(region_def["mapping"]) * 2:
        print("WARNING "
            f"not all gene in {region_def['id']} are found in "
            f"record {record.id}", file=stderr)
    start = min(f.location.start for f in found_features)
    end = max(f.location.end for f in found_features)
    region_record = record[start:end]
    region_record.name = region_record.id = region_def["id"]
    region_record.description = \
        f"{region_def['locus']} from {region_def['orgn']}"
    for ann in ["molecule_type", "source", "organism", "taxonomy"]:
        region_record.annotations[ann] = record.annotations[ann]
    region_record.annotations["topology"] = "linear"
    region_record.annotations["accessions"] = [region_def["id"]]
    region_record.annotations["sequence_version"] = 1
    region_record.annotations["structured_comment"] = {
        "rKnownCGC-Data" : {
            "Locus name"         : region_def["locus"],
            "Original accession" : record.annotations["accessions"][0]
        },
        "antiSMASH-Data" : {
            "Version"            : "6.1.1"
        }
    }
    return region_record

def edit_region_genes(record: SeqRecord, region_def: dict):
    """Edit genes and CDS in a rKnownCGC region"""
    proteins = region_def["proteins"] = dict()
    discard = []
    for feature in record.features:
        name = find_identifier(feature, region_def)
        if name is None:
            discard.append(feature)
        else:
            func, as_name = region_def["mapping"][name]
            # edit region
            if as_name is None:
                as_name = name
            feature.qualifiers["gene"] = [as_name]
            feature.qualifiers["gene_kind"] = [func]
            # update definition -> proteins
            if feature.type == "CDS":
                proteins[as_name] = {
                    "pid" : feature.qualifiers["protein_id"][0],
                    "product" : "",
                    "start" : feature.location.start,
                    "end" : feature.location.end,
                    "seq" : feature.qualifiers["translation"][0]
                }
                for i in ["product", "note"]:
                    if i in feature.qualifiers:
                        proteins[as_name]["product"] += \
                        feature.qualifiers[i][0]
    for feature in discard:
        record.features.remove(feature)

def edit_region_clusters(record: SeqRecord, region_def: dict):
    """Add antiSMASH-like cluster features to a region record"""
    # init a list for candidates
    # {proto: set(int), start : int, end : int, genes : {name}}
    # also used in writing cluster list
    cands = region_def["cand"] = []
    
    proteins = region_def["proteins"]

    # label all protoclusters and save info for "single" kind candidates
    def find_range(gene_list):
        start = min(proteins[n]["start"] for n in gene_list)
        end = max(proteins[n]["end"] for n in gene_list)
        return start, end
    def location(start:int, end:int, strand=1) -> FeatureLocation:
        return FeatureLocation(ExactPosition(start),
                               ExactPosition(end),
                               strand=strand)
    for i, cluster in enumerate(region_def["clusters"]):
        start, end = find_range(cluster["genes"])
        substrate = cluster["substrate"]
        product = cluster["class"]
        if product is None:
            product = "Unclassified"
        proto_num = i+1
        # label protocluster and proto_core
        record.features.insert(0, SeqFeature(
            location=location(start, end), type="proto_core",
            qualifiers={"protocluster_number" : proto_num,
                        "substrate" : substrate, "product" : product}))
        record.features.insert(0, SeqFeature(
            location=location(start, end), type="protocluster",
            qualifiers={"protocluster_number" : proto_num,
                        "category" : cluster["category"],
                        "substrate" : substrate, "product" : product}))
        # save a "single" type candidate cluster into region_def
        cands.append({
                "proto" : set([proto_num]),
                "start" : start, "end" : end,
                "genes" : set(cluster["genes"]),
                "prod" : set([cluster["substrate"]]),
                "kind" : "single",
            })

    # generate combined candidate clusters
    def mergeto(c1, c2):
        c1["proto"].update(c2["proto"])
        c1["start"] = min(c1["start"], c2["start"])
        c1["end"] = max(c1["end"], c2["end"])
        c1["genes"].update(c2["genes"])
        c1["prod"].update(c2["prod"])

    if len(cands) > 1:
        # chemical_hybrid kind
        # share genes or location containing
        cands.sort(key=lambda c: (c["start"], -c["end"]))
        i = 0
        while i < len(cands) - 1:
            c1 = cands[i]
            j = i + 1
            while j < len(cands) - 1:
                c2 = cands[j]
                if \
                set.intersection(c1["genes"], c2["genes"]) or \
                c1["end"] > c2["end"]:
                    mergeto(c1, c2)
                    c1["kind"] = "chemical_hybrid"
                    cands.remove(c2)
                else:
                    j += 1
            i += 1
        # interleaved kind
        # location overlapping
        cands.sort(key=lambda c: c["start"])
        i = 0
        while i < len(cands) - 1:
            c1 = cands[i]
            if c1["kind"] == "single":
                j = i + 1
                while j < len(cands) - 1:
                    c2 = cands[j]
                    if \
                    c2["kind"] == "single" and \
                    c1["end"] > c2["start"]:
                        mergeto(c1, c2)
                        c1["kind"] = "interleaved"
                        cands.remove(c2)
                    else:
                        j += 1
            i += 1
        # make a big neighbouring kind candidate
        if len(cands) > 1:
            cands.sort(key=lambda c: c["start"])
            cands.append({
                    "proto" : set.union(*[c["proto"] for c in cands]),
                    "start" : min(c["start"] for c in cands),
                    "end" : max(c["start"] for c in cands),
                    "genes" : set.union(*[c["genes"] for c in cands]),
                    "prod" : set.union(*[c["prod"] for c in cands]),
                    "kind" : "neighbouring"
                })

    # label candidates in record
    for i, c in enumerate(cands):
        record.features.insert(0, SeqFeature(
            location=location(c["start"], c["end"]), type="cand_cluster",
            qualifiers={"candidate_cluster_number" : i+1,
                        "protoclusters" : list(c["proto"]),
                        "product"  : list(c["prod"]),
                        "kind" : c["kind"]}))

    # label whole region
    record.features.insert(0, SeqFeature(
        location=location(0, len(record)), type="region",
        qualifiers={"region_number" : i+1,
                    "candidate_cluster_numbers" : \
                        [i+1 for i in range(len(cands))],
                    "product" : list(
                        set(c["substrate"] for c in region_def["clusters"]))}))

def write_fasta_records(record, handle: TextIO):
    """Extract CDS translation from region record"""
    rcgcid = record.id
    if rcgcid.endswith("0"):
        for feature in record.features:
            if feature.type == "CDS" and "gene_kind" in feature.qualifiers:
                s, e = feature.location.start, feature.location.end
                strand = {1 : "+", -1 : "-"}[feature.strand]
                tag = feature.qualifiers["protein_id"][0]
                name = feature.qualifiers["gene"][0]
                product = []
                if "gene" in feature.qualifiers:
                    product.append(f"({feature.qualifiers['gene'][0]})")
                if "product" in feature.qualifiers:
                    product.append(feature.qualifiers['product'][0])
                if "note" in feature.qualifiers:
                    product.append(feature.qualifiers['note'][0])
                product = " ".join(product)
                prompt = f">{rcgcid}|c1|{s}-{e}|{strand}|{tag}|{product}|{name}"
                print(prompt, file=handle)
                print(feature.qualifiers["translation"][0], file=handle)

def write_cluster_list(region_def: dict, rcgcid: str, handle: TextIO):
    """Append cluster information line to cluster list file"""
    rcgcid = record.id
    locus_name = region_def["locus"]
    organism = region_def["orgn"]
    for feature in record.features:
        if feature.type == "cand_cluster":
            substrate = ".".join(feature.qualifiers["product"])
            cluster_info = f"{substrate} | {locus_name} | {organism}"


    pid = ";".join([f.qualifiers["protein_id"][0]
                    for f in record.features
                    if f.type == "CDS" and "gene_kind" in f.qualifiers])
    print(f"{rcgcid}\t{rcgcinfo}\tc1\t{category}\t{pid}\t{pid}",
          file=handle)

def load_region_definition(definition_dir : str) -> dict:
    """Load region definition yamls and merge by source accessions"""
    rcgcids = [fn.split(".")[0]
                for fn in listdir(definition_dir)
                if fn.startswith("RCGC") and fn.endswith(".yaml")]
    ret = dict()
    with tqdm(rcgcids, desc="Parsing Definition") as pbar:
        for rcgcid in pbar:
            with open(f"{definition_dir}/{rcgcid}.yaml", "r") as fi:
                pbar.set_postfix({"Region" : rcgcid})
                region_def = yaml.safe_load(fi)
                if region_def["ncbi"] not in ret:
                    ret[region_def["ncbi"]] = {rcgcid: region_def}
                else:
                    ret[region_def["ncbi"]][rcgcid] = region_def
    return ret

from tqdm import tqdm

if __name__ == '__main__':
    definitions = load_region_definition(definition_dir)
    fasta_out = open(f"{fasta_dir}/proteins.fasta")
    list_out = open(f"{fasta_dir}/proteins.fasta")
    with tqdm(definitions.keys()) as pbar:
        for accession in pbar:
            pbar.set_description(f"Extracting from Source {accession}")
            source_record = load_record_from_ncbi(accession, source=source_dir)
            for rcgcid in definitions[accession]:
                pbar.set_postfix({"Region": rcgcid})
                region_def = definitions[accession][rcgcid]
                region_def["id"] = rcgcid
                region_record = clip_region(source_record, region_def)
                edit_region_genes(region_record, region_def)
                edit_region_clusters(region_record, region_def)
                # write_fasta_records(region_record, fasta_out)
                # write_cluster_list(region_def, rcgcid, list_out)
                SeqIO.write(region_record,
                    f"{genbank_dir}/{rcgcid}.gbk", "gb")
