#!/usr/bin/env bash

# help doc
if [[ $# -eq 0 ]] ; then
	echo "bash compress-db.sh [version]"
	echo "Arguments:"
	echo "    version   string, version name, (e.g. \"1.0\")"
	exit
fi

ver=${1}

diamond makedb --in proteins.fasta --db proteins --out proteins.dmnd
if ! [[ -d ${ver} ]] ; then
	mkdir ${ver}
fi
cp proteins.fasta proteins.dmnd clusters.txt ${ver}/
tar -cJf rknowncgc_${ver}.tar.xz ${ver}

echo "Archive Checksum = "$( shasum -a 256 rknowncgc_${ver}.tar.xz )
echo "Fasta Checksum   = "$( shasum -a 256 ${ver}/proteins.fasta )