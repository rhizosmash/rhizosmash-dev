#!/usr/bin/env python3
"""Distribute gene clusters into directories of their own substrate label

Usage:
    python3 prepare-input.py prefix

Arguments:
    prefix (str) the input directory name inside bigscape-input/
"""

from sys import argv, stderr
from Bio import SeqIO

from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import ExactPosition, SeqFeature, FeatureLocation

from typing import List, Tuple
from os import mkdir, listdir
from os.path import exists, isdir, splitext

def find_clusters(record: SeqRecord) -> List[Tuple[str, SeqRecord]]:
    """Find cluster cores in a SeqRecord and return the trimmed in a list"""
    trimmed = list()
    for feature in record.features:
        if feature.type == "protocluster":
            location = feature.location
            substrate = feature.qualifiers["product"][0]
            s, e = location.start, location.end
            trimmed_record = record[s:e]
            # clean up features
            for tf in trimmed_record.features:
                if not tf.type in ["gene", "CDS"]:
                    trimmed_record.features.remove(tf)
            # add back cluster region
            trimmed_record.features.append(SeqFeature(
                location=FeatureLocation(ExactPosition(0),
                                         ExactPosition(len(trimmed_record)),
                                         strand=1),
                type="region",
                qualifiers={"substrate" : substrate,
                            "substrate_category" : feature.qualifiers["category"][0]}))
            # add annotations
            for key in ["molecule_type", "topology", "source", "organism", "taxonomy"]:
                trimmed_record.annotations[key] = record.annotations[key]
            # return substrate and trimmed record
            trimmed.append((substrate, trimmed_record))
    if len(trimmed):
        return trimmed
    else:
        raise ValueError(f"Cluster core not found in record [{record.id}]")

if __name__ == '__main__':
    if len(argv) != 2 or "-h" in argv or "--help" in argv:
        print(__doc__, file=stderr)
        exit()
    input_dir = "bigscape-input/" + argv[1]
    output_dir = input_dir + "-split"
    if not (exists(input_dir) and isdir(input_dir)):
        raise ValueError(f"Input directory not found [{input_dir}]")
    else:
        if not exists(output_dir):
            mkdir(output_dir)
        for file_name in listdir(input_dir):
            input_file_path = f"{input_dir}/{file_name}"
            base, ext = splitext(file_name)
            record = SeqIO.read(input_file_path, "genbank")
            for i, trimmed in enumerate(find_clusters(record)):
                substrate, trimmed_record = trimmed
                if not exists(f"{output_dir}/{substrate}"):
                    mkdir(f"{output_dir}/{substrate}")
                output_file_path = f"{output_dir}/{substrate}/{base}-{i+1}{ext}"
                with open(output_file_path, "w") as f:
                    SeqIO.write(trimmed_record, f, "genbank")
