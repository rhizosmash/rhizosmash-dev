#!/usr/bin/env bash

# help doc
if [[ $# -eq 0 ]] ; then
	echo "bash collect-clusters.sh [container] [prefix]"
	echo "Arguments:"
	echo "    container  directory, contains output directories from rhizosmash"
	echo "    prefix     string, directory for input files"
	exit
fi

container=${1}
prefix=${2}

if ! [[ -d ${container} ]] ; then
	echo "ERROR container directory ${container} not found" >&2
	exit
fi

if ! [[ -d bigscape-input/${prefix} ]] ; then
	mkdir bigscape-input/${prefix}
fi

cp ${container}/*/*.region*.gbk bigscape-input/${prefix}/