#!/usr/bin/env bash

# help doc
if [[ $# -eq 0 ]] ; then
    echo "bash run-bigscape.sh [-s] prefix config"
    echo "Options:"
    echo "    -s run bigscape separately for subdirectories"
    echo "Arguments:"
    echo "    prefix   string, directory for input/output files"
    echo "    config   text file, contains arguments passed to bigscape"
    exit
fi

# parse opt and args
if [[ ${1} = "-s" ]] ; then
    separate=yes
    shift
fi
prefix=${1}
config=${2}

# detect bigscape
if [[ -z $(which bigscape) ]] ; then
    echo 'ERROR bigscape not found in environment' >&2
    exit 1
fi

# get config
if [[ -f ${config} ]] ; then
    bigscape_args=$(tr '\n' ' ' < ${config})
else
    echo "ERROR config file not found" >&2
    exit
fi

# run bigscape
if ! [[ -d bigscape-input/${prefix} ]] ; then
    echo "ERROR input directory bigscape-input/${prefix} not found" >&2
    exit
else
    if ! [ -d bigscape-output/${prefix} ] ; then
        mkdir bigscape-output/${prefix}
    fi
    if [[ ${separate} = yes ]] ; then
        for substrate in $( ls bigscape-input/${prefix} ); do
            echo -n "Running: bigscape -i bigscape-input/${prefix}/${substrate} "
            echo -n "-o bigscape-output/${prefix}/${substrate} "
            echo ${bigscape_args}
            bigscape -i bigscape-input/${prefix}/${substrate} \
                     -o bigscape-output/${prefix}/${substrate} \
                     ${bigscape_args}
        done
    else
        echo -n "Running: bigscape -i bigscape-input/${prefix} "
        echo "-o bigscape-output/${prefix} ${bigscape_args}"
        bigscape -i bigscape-input/${prefix} -o bigscape-output/${prefix} \
                 ${bigscape_args}
    fi
fi