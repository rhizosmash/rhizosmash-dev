#!/usr/bin/env python3
"""Collect and summarize domains based on rhizosmash and bigscape output

Usage:
    python3 summarize-domains.py prefix fam_cut, clan_cut, dom_cut, tree_cut

Arguments:
    prefix (str) the input directory name inside bigscape-input/ and
                 bigscape-output/
    fam_cut, clan_cut (float, 0-1) the cutoff values used in family
                 and clan assignment
    dom_cut (float, 0-1) the cutoff ratio to keep a domain
    tree_cut (float, 0-1) the cutoff ratio to determain comon taxa
"""

from sys import argv, stderr
from Bio import SeqIO

from Bio.SeqRecord import SeqRecord

from os import mkdir, listdir
from os.path import exists, isdir, splitext

import pandas as pd

substrate_list = {
    "Xylose":"SU", "Mannitol":"SU", "Inositol":"SU",
    "Trehalose_PTS":"SU", "Trehalose_trehalase":"SU", "Trehalose-6-P_phosphorylase":"SU",
    "Sucrose_levansucrase":"SU", "Sucrose_phosphorylase":"SU",
    "Sucrose-6-P_hydrolase":"SU", "Xylan":"SU",

    "Glutarate_hydroxylase":"OA", "Glutarate_CoA-ligase":"OA",
    "3-Oxoadipate":"OA", "2-Oxopentenoate":"OA",
    "L-Threonate":"OA", "D-Threonate":"OA",

    "L-Proline":"AA", "D-Proline":"AA",
    "L-Lysine_carboxylase":"AA", "L-Lysine_monooxygenase":"AA",
    "L-Lysine_e-aminotransferase":"AA", "D-Lysine":"AA",
    "Glutamine_glutaminase":"AA", "Glutamine_glutamate-synthase":"AA",
    "Glutathione_Glutamine_Glutamate":"AA", "Tryptophan_monooxygenase":"AA",
    "Pipecolate":"AA", "GABA_AMV":"AA", "Indolepyruvate_decarboxylase":"AA",

    "Phenylethylamine":"AM", "Tyramine":"AM", "Tyramine_Dopamine":"AM",
    "Polyamine_putrescine-like":"AM", "Agmatine":"AM",

    "Phenylacetate":"SE",
    "Indoleacetate_iac":"SE", "Indoleacetate_iad":"SE", "Indoleacetate_iaa":"SE",
    "Salicylate_1-hydroxylase":"SE", "Salicylate_5-hydroxylase":"SE",
    "Salicylate_CoA-ligase":"SE",
    "Benzoate":"SE", "Quinate_Shikimate":"SE", "4-Hydroxyphenylacetate":"SE",
    "Protocatechuate_ortho":"SE", "Protocatechuate_para":"SE",
    "Catechol_meta":"SE", "Catechol_ortho":"SE", "Nicotinate":"SE", "Gentistate":"SE",
    "Anthranilate":"SE", "Naphthalene":"SE", "Toluene":"SE", "Vanillate":"SE"
}

__latest_run_dirs = dict()
def find_latest_run(dir: str):
    """Return the network_files dir of the latest run"""
    if dir in __latest_run_dirs:
        return __latest_run_dirs[dir]
    else:
        network_files_path = f"{dir}/network_files"
        runs = listdir(network_files_path)
        time_points = [i.split("_")[:2] for i in runs]
        time_points = [i[0].split("-") + i[1].split("-") for i in time_points]
        last_run = sorted([run for _, run in zip(time_points, runs)], reverse=True)[0]
        return f"{network_files_path}/{last_run}"

def collect_bigscape(dir: str, table: dict, doms: dict):
    """Collect domain tables from bigscape output directory
    
    Update table {cluster: {short name : max score} }
    Update set   {short name : T/F core}
    """
    # find "domtable" directory inside bigscape output directory
    domain_table_dir = f"{dir}/cache/domtable"
    if not (exists(domain_table_dir) and isdir(domain_table_dir)):
        raise ValueError(f"Domain table directory not found [{domain_table_dir}]")
    table_list = listdir(domain_table_dir)
    # parse domain table files
    for file_name in table_list:
        # get cluster id
        cluster, ext = splitext(file_name)
        if not cluster.startswith("RCGC"):
            if cluster not in table:
                table[cluster] = dict()
            with open(f"{domain_table_dir}/{file_name}", "r") as f:
                for line in f:
                    if not line.startswith("#"):
                        info = line.strip().split()
                        short_name, score = info[0], float(info[13])
                        # update domain set and table
                        doms[short_name] = False
                        if short_name not in table[cluster] \
                        or score > table[cluster][short_name]:
                            table[cluster][short_name] = score

def collect_rhizosmash(dir: str, table: dict, doms: dict):
    """Collect domains from rhizosmash output directory and make domain table
    
    Update table {cluster: {short name : max score} }
    Update set   {short name : T/F core}
    """
    gbk_list = [f for f in listdir(dir) if f.endswith(".gbk")]
    for file_name in gbk_list:
        cluster, ext = splitext(file_name)
        if cluster not in table:
            table[cluster] = dict()
        # parse cluster file
        record = SeqIO.read(f"{dir}/{file_name}", "gb")
        for f in record.features:
            if f.type == "CDS" and "gene_kind" in f.qualifiers \
                and "catabolic" in f.qualifiers["gene_kind"]:
                for info in f.qualifiers["sec_met_domain"]:
                    short_name = info.split(" (")[0]
                    score = float(info.split("bitscore: ")[1].split(", ")[0])
                    # update domain set and table
                    doms[short_name] = True
                    if short_name not in table[cluster] \
                    or score > table[cluster][short_name]:
                        table[cluster][short_name] = score

def parse_network(dir: str, category: str, fam_cutoff: float=0.4, clan_cutoff: float=0.6):
    """Return {clan: {fam: [clusters]}}"""
    run_path = find_latest_run(dir)
    path = f"{run_path}/{category}"
    # init output
    network = dict()
    # parse cluster, family and clan from file
    if isdir(path):
        fam_file = f"{path}/{category}_clans_{fam_cutoff:.2f}_{clan_cutoff:.2f}.tsv"
        use_clan = True
        if not exists(fam_file):
            fam_file = f"{path}/{category}_clustering_c{fam_cutoff:.2f}.tsv"
            use_clan = False
        if not exists(fam_file):
            raise ValueError(f"Input file not found [{fam_file}]")
        with open(fam_file, "r") as f:
            for line in f:
                if not line.startswith("#"):
                    if use_clan:
                        cluster, clan, fam = line.strip().split()
                    else:
                        cluster, fam = line.strip().split()
                        clan = category
                    fam = f"FAM_{int(fam):05d}"
                    clan = f"CLAN_{clan}"
                    if clan in network:
                        if fam in network[clan]:
                            network[clan][fam].append(cluster)
                        else:
                            network[clan][fam] = [cluster]
                    else:
                        network[clan] = {fam:[cluster]}
    return network

def find_common_taxa(dir: str, network: dict, tree_cutoff: float):
    """Returns the most detailed common taxa in each family"""
    class Node:
        def __init__(self):
            self.count = 0
            self.leaves = dict()
    class Taxa_Tree:
        """A simple taxanomy tree structure to find common taxa level"""
        def __init__(self):
            self.body = Node()

        def add(self, linage:list):
            pp = self.body
            for tx in linage:
                if tx not in pp.leaves:
                    pp.leaves[tx] = Node()
                pp.count += 1
                pp = pp.leaves[tx]
            pp.count += 1
        def common(self, cutoff:float):
            pp = self.body
            common = []
            crt = pp.count * cutoff
            while len(pp.leaves):
                mx, tx = sorted([(nd.count, tx) for tx, nd in pp.leaves.items()],
                                reverse=True)[0]
                if mx < crt:
                    break
                common.append(tx)
                pp = pp.leaves[tx]
            if len(pp.leaves):
                leaves = sorted([(nd.count, tx) for tx, nd in pp.leaves.items()],
                                reverse=True)[:3]
                tx = "|".join(tx for _, tx in leaves)
                if len(pp.leaves) > 3:
                    tx += "|..."
                common.append(tx)
            return ", ".join(common[-2:])
    # find and parse annotation file
    run_path = find_latest_run(dir)
    annotation_file = f"{run_path}/Network_Annotations_Full.tsv"
    records = dict()
    with open(annotation_file, "r") as f:
        next(f)
        for line in f:
            line = line.strip().split("\t")
            cl, taxa = line[0], line[-1]
            records[cl] = taxa.split(",")
    # union taxa lists
    common = dict()
    for fc in network.values():
        for fam, clusters in fc.items():
            taxa = Taxa_Tree()
            for cl in clusters:
                taxa.add(records[cl])
            common[fam] = taxa.common(tree_cutoff)
    return common

def summarize_domain_table(table: dict, doms: dict, dom_cutoff: float,
                           network: dict, taxa: dict):
    """Returns summarized domain table"""
    result = []
    domain_list = list(doms)
    column_names = ["Clan", "Family", "NCluster", "rKnown", "Taxa"] + domain_list
    for clan in network:
        for fam, clusters in network[clan].items():
            if not all(cl.startswith("RCGC") for cl in clusters):
                count = len(clusters)
                rknown = any(cl.startswith("RCGC") for cl in clusters)
                clusters = [cl for cl in clusters if not cl.startswith("RCGC")]
                scores = [[table[cl][dom] for cl in clusters if dom in table[cl]]
                          for dom in domain_list]
                scores = [min(ss) if len(ss) else None for ss in scores]
                result.append([clan, fam, count, rknown, taxa[fam]] + scores)
    table = pd.DataFrame(result, columns=column_names).set_index(["Clan", "Family"])
    nulls = table.loc[:, domain_list].isnull().sum(0)
    domain_list = [(doms[d], -n, d) for d, n in zip(domain_list, nulls)]
    domain_list = [d for _, mn, d in sorted(domain_list, reverse=True)
                     if doms[d] or len(table) + mn >= dom_cutoff * len(table)]
    table = table.loc[:, ["NCluster", "rKnown", "Taxa"] + domain_list]
    table.rename(columns=dict((n,f"[{n}]") if doms[n] else (n,n) for n in doms),
                 inplace=True)
    return table

if __name__ == '__main__':
    if len(argv) != 6 or "-h" in argv or "--help" in argv:
        print(__doc__, file=stderr)
        exit()
    bigscape_dir = "bigscape-output/" + argv[1]
    rhizo_dir = "bigscape-input/" + argv[1]
    fam_cutoff, clan_cutoff = float(argv[2]), float(argv[3])
    dom_cutoff, tree_cutoff = float(argv[4]), float(argv[5])
    
    if not (exists(rhizo_dir) and isdir(rhizo_dir)):
        raise ValueError(f"Input directory not found [{rhizo_dir}]")
    if not (exists(bigscape_dir) and isdir(bigscape_dir)):
        raise ValueError(f"Input directory not found [{bigscape_dir}]")
    substrate_list = {sub:substrate_list[sub]
                      for sub in substrate_list
                      if exists(f"{rhizo_dir}/{sub}")}
    
    for sub in substrate_list:
        if len([c for c in listdir(f"{rhizo_dir}/{sub}") if not c.startswith(".")]) > 1:
            print(f"Collecting [{sub}]", file=stderr)
            dom_table, dom_set = dict(), dict()
            collect_bigscape(dir=f"{bigscape_dir}/{sub}",
                             table=dom_table,
                             doms=dom_set)
            collect_rhizosmash(dir=f"{rhizo_dir}/{sub}",
                               table=dom_table,
                               doms=dom_set)
            network = parse_network(dir=f"{bigscape_dir}/{sub}",
                                    category=substrate_list[sub],
                                    fam_cutoff=fam_cutoff, clan_cutoff=clan_cutoff)
            common_taxa = find_common_taxa(dir=f"{bigscape_dir}/{sub}",
                                           network=network,
                                           tree_cutoff=tree_cutoff)
            result = summarize_domain_table(table=dom_table,
                                            doms=dom_set,
                                            dom_cutoff=dom_cutoff,
                                            network=network,
                                            taxa=common_taxa)
            with pd.ExcelWriter(f"{bigscape_dir}/{sub}/summary.xlsx") as xw:
                result.to_excel(xw, sheet_name="Summary", freeze_panes=(1,5))

        else:
            print(f"Skip [{sub}]", file=stderr)
        # write_summary(summary=result, dir=f"{bigscape_dir}/{sub}")