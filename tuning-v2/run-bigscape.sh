#!/usr/bin/env bash

# help doc
if [[ $# -eq 0 ]] ; then
    echo "bash run-bigscape.sh [-h]"
    echo "                     [-i input-dir -o output-dir -r reference-dir"
    echo "                      -p pfam -c config -t types]"
    echo "Options:"
    echo "  -i -o -r directories   directory path send to bigscape"
    echo "  -p       pfam          path to pfam database"
    echo "  -c       config        config file path"
    echo "  -t       types         a file in which each line is a cluster type name"
    echo "  -h                     print help message and exit"
    exit
fi

# parse options
input_dir=""
output_dir=""
reference_dir=""
pfam=""
config=""
types=""
while getopts ":i:o:r:p:c:t:h" opt ; do
    case ${opt} in
        i) input_dir=${OPTARG} ;;
        o) output_dir=${OPTARG} ;;
        r) reference_dir=${OPTARG} ;;
        p) pfam=${OPTARG} ;;
        c) config=${OPTARG} ;;
        t) types=${OPTARG} ;;
        h|*)  bash $0 ; exit ;;
        :) echo "ERROR option -${OPTARG} requires an argumant" ; bash $0 ; exit ;;
    esac
done

# check input
if [[ -z ${input_dir} || -z ${output_dir} || -z ${reference_dir} \
   || -z ${pfam} || -z ${config} || -z ${types} ]] ; then
    echo "ERROR options -iorpct are mandatory"
    bash $0
    exit
fi

# detect bigscape
if [[ -z $(which bigscape) ]] ; then
    echo 'ERROR bigscape not found in environment' >&2
    exit
fi

# get config
if [[ -f ${config} ]] ; then
    bigscape_args=$(tr '\n' ' ' < ${config})
else
    echo "ERROR config file not found" >&2
    exit
fi

# run bigscape
cat ${types} | while read type ; do
    echo "RUNNING cluster type ${type}"
    bigscape cluster -i ${input_dir} -o ${output_dir} \
        -r ${reference_dir} ${bigscape_args} \
        -p ${pfam} \
        --include_classes ${type} --label ${type}
done
